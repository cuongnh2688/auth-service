package com.omg.auth.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenDTO {

    private String accessToken;

    private String refreshToken;

    private String userId;
}

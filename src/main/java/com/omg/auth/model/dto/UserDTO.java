package com.omg.auth.model.dto;

import com.omg.auth.document.Users;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDTO {

    private String id;

    private String username;

    public static UserDTO from(Users user) {
        return UserDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .build();
    }
}

package com.omg.auth.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenRequest {

    private String refreshToken;
}

package com.omg.auth.service;

import com.omg.auth.model.dto.UserDTO;

public interface UserService {
    UserDTO getUserInfo(String id);
}

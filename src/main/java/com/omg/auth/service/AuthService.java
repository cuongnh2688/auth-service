package com.omg.auth.service;

import com.omg.auth.model.dto.TokenDTO;
import com.omg.auth.model.request.LoginRequest;
import com.omg.auth.model.request.SignupRequest;
import com.omg.auth.model.request.TokenRequest;

public interface AuthService {
    TokenDTO registerAndCreateToken(SignupRequest request);

    TokenDTO login(LoginRequest request);

    TokenDTO token(TokenRequest request);
}

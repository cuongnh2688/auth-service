package com.omg.auth.service.impl;

import com.omg.auth.document.Users;
import com.omg.auth.jwt.TokenGenerator;
import com.omg.auth.model.dto.TokenDTO;
import com.omg.auth.model.request.LoginRequest;
import com.omg.auth.model.request.SignupRequest;
import com.omg.auth.model.request.TokenRequest;
import com.omg.auth.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.resource.BearerTokenAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private UserManager userManager;

    @Autowired
    private TokenGenerator tokenGenerator;

    @Autowired
    private DaoAuthenticationProvider provider;

    @Autowired
    @Qualifier("jwtRefreshTokenAuthProvider")
    private JwtAuthenticationProvider jwtRefreshTokenAuthProvider;

    @Override
    public TokenDTO registerAndCreateToken(SignupRequest request) {

        Users user = new Users(request.getUsername(), request.getPassword());
        this.userManager.createUser(user);

        Authentication authentication =
                UsernamePasswordAuthenticationToken.authenticated(user, request.getPassword(), Collections.EMPTY_LIST);

        return this.tokenGenerator.createToken(authentication);
    }

    @Override
    public TokenDTO login(LoginRequest request) {

        Authentication authentication =
                this.provider.authenticate(
                        UsernamePasswordAuthenticationToken.unauthenticated(request.getUsername(), request.getPassword())
                );

        return this.tokenGenerator.createToken(authentication);
    }

    @Override
    public TokenDTO token(TokenRequest request) {
        Authentication authentication = this.jwtRefreshTokenAuthProvider.authenticate(
                new BearerTokenAuthenticationToken(request.getRefreshToken())
        );
        return this.tokenGenerator.createToken(authentication);
    }
}

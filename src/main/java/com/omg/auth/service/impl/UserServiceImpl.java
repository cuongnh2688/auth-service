package com.omg.auth.service.impl;

import com.omg.auth.model.dto.UserDTO;
import com.omg.auth.repository.UsersRepository;
import com.omg.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDTO getUserInfo(String id) {

        return UserDTO.from(this.usersRepository.findById(id).orElseThrow());
    }
}

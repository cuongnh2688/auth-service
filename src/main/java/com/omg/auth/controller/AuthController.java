package com.omg.auth.controller;

import com.omg.auth.model.dto.TokenDTO;
import com.omg.auth.model.request.LoginRequest;
import com.omg.auth.model.request.SignupRequest;
import com.omg.auth.model.request.TokenRequest;
import com.omg.auth.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody SignupRequest request) {

        return ResponseEntity.ok(this.authService.registerAndCreateToken(request));
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody LoginRequest request) {

        return ResponseEntity.ok(this.authService.login(request));
    }

    @PostMapping("/token")
    public ResponseEntity token(@RequestBody TokenRequest request) {

        return ResponseEntity.ok(this.authService.token(request));
    }
}

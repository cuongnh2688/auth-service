package com.omg.auth.controller;

import com.omg.auth.document.Users;
import com.omg.auth.model.dto.UserDTO;
import com.omg.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    @PreAuthorize("#user.id == #id")
    public ResponseEntity getUserInfo(@AuthenticationPrincipal Users user, @PathVariable("id") String id) {

        return ResponseEntity.ok(this.userService.getUserInfo(id));
    }
}

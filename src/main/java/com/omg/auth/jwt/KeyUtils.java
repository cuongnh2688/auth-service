package com.omg.auth.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Objects;

@Component
public class KeyUtils {

    @Autowired
    private Environment environment;

    @Value("${access-token.private}")
    private String accessTokenPrivateKeyPath;

    @Value("${access-token.public}")
    private String accessTokenPublicKeyPath;

    @Value("${refresh-token.private}")
    private String refreshTokenPrivateKeyPath;

    @Value("${refresh-token.public}")
    private String refreshTokenPublicKeyPath;

    private KeyPair _accessTokenKeyPair;

    private KeyPair _refreshTokenKeyPair;

    private KeyPair getAccessTokenKeyPair() {

        if(Objects.isNull(this._accessTokenKeyPair)) {
            this._accessTokenKeyPair = this.getKeyPair(this.accessTokenPublicKeyPath, this.accessTokenPrivateKeyPath);
        }

        return this._accessTokenKeyPair;
    }

    private KeyPair getRefreshTokenKeyPair() {

        if (Objects.isNull(this._refreshTokenKeyPair)) {
            this._refreshTokenKeyPair = this.getKeyPair(this.refreshTokenPublicKeyPath, this.refreshTokenPrivateKeyPath);
        }

        return this._refreshTokenKeyPair;
    }

    private KeyPair getKeyPair(String publicKeyPath, String privateKeyPath) {

        File publicKeyFile = new File(publicKeyPath);
        File privateKeyFile = new File(privateKeyPath);

        // Get key pair in case private and public key files exist.
        if (publicKeyFile.exists() && privateKeyFile.exists()) {
            try {
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");

                // Read public key file and generate public key
                byte[] publicKeyBytes = Files.readAllBytes(publicKeyFile.toPath());
                EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
                PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

                // Read private key file and generate public key
                byte[] privateKeyBytes = Files.readAllBytes(privateKeyFile.toPath());
                PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
                PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

                // create a key pair
                return new KeyPair(publicKey, privateKey);
            } catch (NoSuchAlgorithmException | IOException | InvalidKeySpecException e) {
                throw new RuntimeException(e);
            }
        }

        // In case public key and private key files do not exist and the environment is production  --> throw exception
        if (Arrays.stream(environment.getActiveProfiles()).anyMatch(e -> e.equals("prod"))) {
            throw new RuntimeException("Public key and private key does not exist.");
        }

        // Generate key pair when private/public key files does not exist and environment is not production
        return this.generateKeyPair(publicKeyPath, privateKeyPath);
    }

    /**
     * Generate key pair in case the public/private key files do not exist
     * @param publicKeyPath
     * @param privateKeyPath
     * @return
     */
    private static KeyPair generateKeyPair(String publicKeyPath, String privateKeyPath) {

        // Create the directory
        File directory = new File("access-refresh-token-keys");
        if (!directory.exists()) {
            directory.mkdir();
        }

        // Generate key pair
        KeyPair keyPair;
        try {

            // Generate key pair
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            keyPair = keyPairGenerator.generateKeyPair();

            // Write public key into directory. Next time it will be the case exist public key
            FileOutputStream fosPublicKey = new FileOutputStream(publicKeyPath);
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(keyPair.getPublic().getEncoded());
            fosPublicKey.write(publicKeySpec.getEncoded());

            // Write private key into directory. Next time it will be the case exist private key
            FileOutputStream fosPrivate = new FileOutputStream(privateKeyPath);
            X509EncodedKeySpec privateKeySpec = new X509EncodedKeySpec(keyPair.getPrivate().getEncoded());
            fosPrivate.write(privateKeySpec.getEncoded());

        } catch (NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e);
        }

        return keyPair;
    }


    public RSAPublicKey getAccessTokenPublicKey() {
        return (RSAPublicKey)this.getAccessTokenKeyPair().getPublic();
    }

    public RSAPrivateKey getAccessTokenPrivateKey() {
        return (RSAPrivateKey)this.getAccessTokenKeyPair().getPrivate();
    }

    public RSAPublicKey getRefreshTokenPublicKey() {
        return (RSAPublicKey)this.getRefreshTokenKeyPair().getPublic();
    }

    public RSAPrivateKey getRefreshTokenPrivateKey() {
        return (RSAPrivateKey)this.getRefreshTokenKeyPair().getPrivate();
    }

}
